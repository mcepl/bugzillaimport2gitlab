#!/usr/bin/env python
# coding: utf-8

import json
import logging
logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.DEBUG)
import os.path
import sys
import urllib
import urllib2
import xml.etree.ElementTree as etree

import xdg.BaseDirectory

from ConfigParser import SafeConfigParser
from collections import OrderedDict
from pprint import pformat, pprint

bugs_dict = OrderedDict()

def get_config(section=None):
    gitlab_config_file = os.path.join(xdg.BaseDirectory.xdg_config_home,
                                      'gitlab')
    conf_parser = SafeConfigParser()
    conf_parser.read(gitlab_config_file)

    if section is not None:
        cur_site = section
    elif conf_parser.has_section('general'):
        cur_site = conf_parser.get('general', 'default')
    else:
        cur_site = conf_parser.sections()[0]

    token = conf_parser.get(cur_site, 'token')
    base_url = conf_parser.get(cur_site, 'base_url')
    return token, base_url

gitlab_token, gitlab_base_url = get_config()

def run_command(api_point, repo, params):
    req = urllib2.Request(url='{0}/projects/{1}/{2}/'.format(
                          gitlab_base_url, repo, api_point))
    req.add_header('PRIVATE-TOKEN', gitlab_token)

    for par in params:
        if isinstance(params[par], unicode):
            params[par] = params[par].encode('utf8')

    req.add_data(urllib.urlencode(params))
    logging.debug('req:\n%s', str(req))
    logging.debug('req:\n%s', req.get_full_url())
    logging.debug('req:\n%s', req.get_data())
    logging.debug('req:\n%s', req.header_items())
    ret = urllib2.urlopen(req)

    logging.debug('retcode = %d', ret.getcode())
    if (ret.getcode() / 100) == 2:
        response = json.load(ret)
        logging.debug('response:\n%s', str(response))

    return response

def get_text(elem, subtag):
    text = elem.find(subtag).text
    if text is not None:
        return elem.find(subtag).text.strip()
    else:
        return None


def create_bug(project, bug):
    logging.debug('description = %s', bug['comments'])
    desc_elem = bug['comments'][0]

    if desc_elem['text'] is not None:
        description = desc_elem['text']
    else:
        description = ''

    if desc_elem['author'] is not None:
        description = desc_elem['author'] + ':\n' + description

    params = {
        'id': project,
        'title': bug['short_desc'],
        'description': description
    }
    return run_command('issues', project, params)


def add_comment(project, id, text):
    logging.debug('text:\n%s', text)
    params = {
        'id': project,
        'issue_id': id,
        'body': text
    }

    logging.debug('project = %d', project)
    logging.debug('id = %d', id)
    return run_command('issues/{0}/notes'.format(id), project, params)


def file_bug(project, bug):
    new_bug = create_bug(project, bug)
    logging.debug('new_bug:\n%s' % pformat(new_bug))
    logging.debug('new_bug ID:\n%d' % int(new_bug['id']))

    if len(bug['comments']) > 1:
        for comment in bug['comments'][1:]:
            add_comment(project, new_bug['id'], comment['text'])


def add_to_sublist(bug, key, empty, add):
    if key not in bug:
        bug[key] = empty
    if isinstance(empty, list):
        bug[key].append(add)
    elif isinstance(empty, set):
        bug[key].add(add)
    else:
        raise ValueError(
            "I can add only to lists and sets, not %s" % type(empty))


def parse_bug(elem):  # noqa:
    bug = {}
    id = int(get_text(elem, 'bug_id'))
    sys.stderr.write('.')
    for subelem in elem:
        # TODO missing support for the following subelements
        # group*, fl ag*, attachment*
        if subelem.tag == 'long_desc':
            add_to_sublist(bug, 'comments', [], {
                'author': get_text(subelem, 'who'),
                'time': get_text(subelem, 'bug_when'),
                'text': get_text(subelem, 'thetext')
            })
        elif subelem.tag == 'keywords':
            add_to_sublist(bug, 'keywords', set(),
                           get_text(subelem, 'keywords'))
        elif subelem.tag == 'dependson':
            add_to_sublist(bug, 'dependson', [],
                           get_text(subelem, 'dependson'))
        elif subelem.tag == 'blocked':
            add_to_sublist(bug, 'blocked', [],
                           get_text(subelem, 'blocked'))
        elif subelem.tag == 'cc':
            add_to_sublist(bug, 'cc', [],
                           get_text(subelem, 'cc'))
        else:
            bug[subelem.tag] = subelem.text
    bugs_dict[id] = bug


def main():

    with open(sys.argv[1]) as inf:
        moz_xml = etree.parse(inf)
        for elem in moz_xml.getroot():
            parse_bug(elem)

# FIXME should explicitly sort by key (lower to higher)
    for bug_id in bugs_dict:
        file_bug(346279, bugs_dict[bug_id])

if __name__ == '__main__':
    main()
